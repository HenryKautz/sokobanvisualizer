/*
Sokoban Visualizer
Copyright 2020 Henry Kautz <henry.kautz@gmail.com>
This program may be freely redistributed and modified for educational uses.
*/



const usenode: boolean = false;
const debug = true;

enum Square {
    space = ' ', wall = '#', player = '@',
    box = '$', goal = '.', playerOngoal = '+', boxOnGoal = '*'
};

const defaultPuzzle = "#####\n" +
    "#   #\n" +
    "# $ #\n" +
    "#+  #\n" +
    "#####\n";

const defaultSolution = "uurDrdL";

enum Move {
    left = 'l', right = 'r', up = 'u', down = 'd',
    pushleft = 'L', pushright = 'R', pushup = 'U', pushdown = 'D',
    gotobox = 'b', moveboxloc = 'm', moveboxcell = 'M'
};

function Delta(m: Move): [number, number] {
    switch (m) {
        case Move.left:
        case Move.pushleft:
            return [0, -1];
        case Move.right:
        case Move.pushright:
            return [0, 1];
        case Move.up:
        case Move.pushup:
            return [-1, 0];
        case Move.down:
        case Move.pushdown:
            return [1, 0];
    }
}

function Delta2Push(dr: number, dc: number): Move {
    if (dr == 1) { return Move.pushdown; }
    if (dr == -1) { return Move.pushup; }
    if (dc == 1) { return Move.pushright; }
    if (dc == -1) { return Move.pushleft; }
    myError('Bad call to Delta2Push');
}

function isPush(m: Move): boolean {
    switch (m) {
        case Move.pushup:
        case Move.pushdown:
        case Move.pushright:
        case Move.pushleft:
            return true;
    }
    return false;
}


function isOpen(sq: Square): boolean {
    switch (sq) {
        case Square.space:
        case Square.goal:
            return true;
        default:
            return false;
    }
}

function isBox(sq: Square): boolean {
    switch (sq) {
        case Square.box:
        case Square.boxOnGoal:
            return true;
        default:
            return false;
    }
}

function isGoal(sq: Square): boolean {
    switch (sq) {
        case Square.goal:
        case Square.boxOnGoal:
        case Square.playerOngoal:
            return true;
        default:
            return false;
    }
}

function clearSquare(sq: Square): Square {
    switch (sq) {
        case Square.wall:
            myError('Bad call to clearSquare');
        case Square.playerOngoal:
        case Square.boxOnGoal:
        case Square.goal:
            return Square.goal;
        default:
            return Square.space;
    }
}

function putPlayer(sq: Square): Square {
    switch (sq) {
        case Square.wall:
            myError('Bad call to putPlayer');
        case Square.goal:
        case Square.boxOnGoal:
        case Square.playerOngoal:
            return Square.playerOngoal;
        default:
            return Square.player;
    }
}

function putBox(sq: Square): Square {
    switch (sq) {
        case Square.wall:
            myError('Bad call to putBox');
        case Square.goal:
        case Square.boxOnGoal:
        case Square.playerOngoal:
            return Square.boxOnGoal;
        default:
            return Square.box;
    }
}

enum DisplayStrings {
    empty = '',
    box = '<sup>&#x13262;</sup>',
    playerleft = '&#x13022;',
    playerright = '&#x13023;',
    playerup = '&#x13020;',
    playerdown = '&#x13013;'
};

// type Step = [number, Move] | [number, Move, number, Move] | [number, Move, number, number]
type Step = [number, Move] | [number, Move, number, number] | [number, Move, number, Move];
type StepList = Step[];

type Loc = [number, number];
type LocList = Loc[];

function loc2string(r: number, c: number): string {
    return 'r' + r + 'c' + c;
}

function myError(msg: string) {
    console.log(msg);
    if (!usenode) {
        window.alert(msg);
    }
    throw msg;
}

function myWarning(msg: string) {
    console.log(msg);
    if (!usenode) {
        document.getElementById('warningBox').innerHTML =
            document.getElementById('warningBox').innerHTML + msg + '<br>\n';
    }
}

function randomInteger(min: number, max: number): number {
    if (max == min) { return min; }
    if (max < min) {
        myError('Bad call to randomInteger(' + min + ',' + max + ')');
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Sokoban {

    rows: number;
    cols: number;
    board: Square[][];
    step: number;
    playerLocs: LocList;
    boxLocs: LocList;
    numberPlayers: number;
    numberGoals: number;
    numberBoxes: number;
    numberBoxesOnGoal: number;
    multibox: boolean = true;

    solution: string;
    solutionPosition: number;
    solutionStepList: StepList;

    originalPuzzle: string;
    originalSolution: string;

    constructor(puzzleString: string) {
        this.originalPuzzle = puzzleString;
        this.fillPuzzle(puzzleString, true);
    }

    solutionStringToStepList(s: string): StepList {
        // Example:
        // 'u2b3r' ==> [[1, Move.up], [2, Move.gotobox, 3, Move.right]]
        let steps = [];
        for (let i = 0; i < s.length; i++) {
            let idString = '';
            while (s[i].match(/[0-9]/)) {
                idString = idString.concat(s[i++]);
            }
            if (i >= s.length) { myError('Bad solution string (A) ' + s); }
            let idNumber = idString.length == 0 ? 1 : Number(idString);
            if (!s[i].match(/[lrudLRUDbmM]/)) { myError('Bad solution string (B) ' + s); }
            let move = <Move>s[i];
            if (move == Move.gotobox) {
                idString = '';
                i++;
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length) { myError('Bad solution string (C) ' + s); }
                let boxIdNumber = idString.length == 0 ? 1 : Number(idString);
                if (!s[i].match(/[LRUD]/)) { myError('Bad solution string (D) ' + s); }
                let dir = <Move>s[i];
                steps.push([idNumber, move, boxIdNumber, dir]);
            }
            else if (move == Move.moveboxloc) {
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let r1 = Number(idString);
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let c1 = Number(idString);
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let r2 = Number(idString);
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let c2 = Number(idString);
                let cell1 = r1 * this.cols + c1;
                let cell2 = r2 * this.cols + c2;
                steps.push([idNumber, Move.moveboxcell, cell1, cell2]);
            }
            else if (move == Move.moveboxcell) {
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let cell1 = Number(idString);
                i++;
                idString = '';
                while (s[i].match(/[0-9]/)) {
                    idString = idString.concat(s[i++]);
                }
                if (i >= s.length || s[i] != ',') { myError('Bad solution string ' + s); }
                let cell2 = Number(idString);
                steps.push([idNumber, Move.moveboxcell, cell1, cell2]);
            }
            else {
                steps.push([idNumber, move]);
            }
        }
        return steps;
    }



    fillPuzzle(puzzleString: string, allocateBoard = false) {
        let sq: Square;

        this.numberBoxes = 0;
        this.numberGoals = 0;
        this.numberBoxesOnGoal = 0;
        this.numberPlayers = 0;
        this.playerLocs = [];
        this.boxLocs = [];
        let puzzleRowStrings: string[] = [];

        if (puzzleString.includes('"')) {
            // alternative format
            puzzleRowStrings = puzzleString.match(/"[^"]+"/g);
            puzzleRowStrings = puzzleRowStrings.map(s => s.replace(/"/g, ''));
        }
        else {
            puzzleRowStrings = puzzleString.split('\n');
        }
        /* filter out comments */
        puzzleRowStrings = puzzleRowStrings.filter(s => s.indexOf('%') != 0);
        /* filter out empty lines */
        puzzleRowStrings = puzzleRowStrings.filter(s => s != "");
        // calculate number rows
        this.rows = puzzleRowStrings.length;

        /* find number of columns in longest row */
        this.cols = 0;
        for (let r=0; r<this.rows; r++){
            let thisLineLength = puzzleRowStrings[r].replace(/[0-9]/g, '').length;
            if (thisLineLength > this.cols) { this.cols = thisLineLength; }
        }
        /* right pad short rows with blanks */
        for (let r=0; r<this.rows; r++){
            let thisLineLength = puzzleRowStrings[r].replace(/[0-9]/g, '').length;
            if (thisLineLength < this.cols) { 
                puzzleRowStrings[r] = puzzleRowStrings[r].padEnd(this.cols - thisLineLength);
             }
        }        

        if (allocateBoard) {
            this.board = new Array(this.rows);
            for (let r = 0; r < this.rows; r++) {
                this.board[r] = new Array(this.cols);
            }
        }

        this.playerLocs = [];
        for (let r = 0; r < this.rows; r++) {
            if (puzzleRowStrings[r].replace(/0-9/g, '').length != this.cols) {
                myError("Unexpected length of row " + r);
            }

            for (let c = 0, k = 0; c < this.cols; c++, k++) {
                let ch = puzzleRowStrings[r][k];
                let idstring = '';
                while (puzzleRowStrings[r][k].match(/[0-9]/)) {
                    idstring = idstring.concat(ch);
                    k++;
                    ch = puzzleRowStrings[r][k];
                }
                let idnumber = Number(idstring); // 0 if no player number

                switch (ch) {
                    case ' ':
                        if (idnumber > 0) { myError('Unexpected integer before character = ' + ch); }
                        sq = Square.space;
                        break;
                    case '#':
                        if (idnumber > 0) { myError('Unexpected integer before character = ' + ch); }
                        sq = Square.wall;
                        break;
                    case '@':
                        sq = Square.player;
                        this.numberPlayers += 1;
                        if (idnumber == 0) {
                            idnumber = this.numberPlayers;
                        }
                        this.playerLocs[idnumber - 1] = [r, c];
                        break;
                    case '$':
                        sq = Square.box;
                        this.numberBoxes += 1;
                        if (idnumber == 0) {
                            idnumber = this.numberBoxes;
                        }
                        this.boxLocs[idnumber - 1] = [r, c];
                        break;
                    case '.':
                        if (idnumber > 0) { myError('Unexpected integer before character = ' + ch); }
                        sq = Square.goal;
                        this.numberGoals++;
                        break;
                    case '+':
                        sq = Square.playerOngoal;
                        this.numberPlayers += 1;
                        if (idnumber == 0) {
                            idnumber = this.numberPlayers;
                        }
                        this.playerLocs[idnumber - 1] = [r, c];
                        break;
                    case '*':
                        sq = Square.boxOnGoal;
                        this.numberBoxes++;
                        this.numberGoals++;
                        this.numberBoxesOnGoal++;
                        if (idnumber == 0) {
                            idnumber = this.numberBoxes;
                        }
                        this.boxLocs[idnumber - 1] = [r, c];
                        break;
                    default:
                        myError('Bad input puzzle character = ' + ch);
                }
                this.board[r][c] = sq;
            }
        }
        for (let i = 0; i < this.playerLocs.length; i++) {
            if (this.playerLocs[i] == undefined) {
                myError('Missing player number' + (i + 1));
            }
        }
        for (let i = 0; i < this.boxLocs.length; i++) {
            if (this.boxLocs[i] == undefined) {
                myError('Missing box number' + (i + 1));
            }
        }

        if (allocateBoard) {
            this.GenerateBoardHTML();
            this.initializeSolutionFromString();
        }
        else {
            this.UpdateAllSquares();
            this.updateNumberBoxesOnGoal();
        }
    }

    PrintBoard() {
        let s: string[] = [];
        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.cols; c++) {
                s.push(this.board[r][c]);
            }
            s.push('\n');
        }
        s.push('\n');
        console.log(s.join(''));
    }

    GenerateBoardHTML() {
        if (usenode) { return; }
        // Create the HTML to display the initial board
        let element: HTMLElement = document.getElementById('sokobanBoard');
        element == null && myError('GenerateBoardHTML cannot find HTML element sokobanBoard');
        let h: string[] = [];
        for (let r: number = 0; r < this.rows; r++) {
            h.push('<tr>\n')
            for (let c: number = 0; c < this.cols; c++) {
                h.push('<td id="sq-', r.toString(), '-', c.toString(),
                    '" onclick="squareHandler(this.id)" class="');
                // calculate square color
                let sq = this.board[r][c];
                if (isGoal(sq)) {
                    h.push('goal');
                }
                else if (sq == Square.wall) {
                    h.push('wall')
                } else {
                    h.push('space');
                }
                h.push('"></td>\n');
            }
            h.push('</tr>\n')
        }
        let tableText = h.join('');
        element.innerHTML = tableText
        this.UpdateAllSquares();
        this.updateNumberBoxesOnGoal();
    }

    UpdateAllSquares() {
        for (let r: number = 0; r < this.rows; r++) {
            for (let c: number = 0; c < this.cols; c++) {
                this.UpdateSquareHTML(r, c);
            }
        }
    }

    UpdateSquareHTML(r: number, c: number, m: Move = Move.up) {
        // Update the displayed cell for the given board position.
        // Optional m:Move determines which player string to use.
        if (usenode) { return; }
        let sq = this.board[r][c];
        let id: string = 'sq-' + r.toString() + '-' + c.toString();
        let s: string;
        let cell: HTMLElement = document.getElementById(id);
        cell == null && myError('UpdateSquareHTML cannot find HTML element ' + id);
        switch (sq) {
            case Square.space:
            case Square.goal:
            case Square.wall:
                s = DisplayStrings.empty;
                break;
            case Square.box:
            case Square.boxOnGoal:
                s = DisplayStrings.box;
                break;
            case Square.player:
            case Square.playerOngoal:
                switch (m) {
                    case Move.left:
                    case Move.pushleft:
                        s = DisplayStrings.playerleft;
                        break;
                    case Move.right:
                    case Move.pushright:
                        s = DisplayStrings.playerright;
                        break;
                    case Move.up:
                    case Move.pushup:
                        s = DisplayStrings.playerup;
                        break;
                    case Move.down:
                    case Move.pushdown:
                        s = DisplayStrings.playerdown;
                        break;
                }
                if (this.playerLocs.length > 1) {
                    for (let i = 0; i < this.playerLocs.length; i++) {
                        if (this.playerLocs[i][0] == r && this.playerLocs[i][1] == c) {
                            s = s + '<div style="color:red;">' + (i + 1) + '</div>';
                            break;
                        }
                    }
                }
                break;
        }
        cell.innerHTML = s;
    }

    // Implement simple breadth first search 
    //  queue: enqueue = push, dequeue = shift
    //  visited: use maps
    //      var visited = new Map();
    //          methods set and has
    //      convert [row,col] to a string to use as key: 'r' + row + 'c' + col
    FindPath(player: number, startr: number, startc: number, endr: number, endc: number): StepList {
        if (startr == endr && startc == endc) { return []; }
        let Visited = new Map();
        let Parent = new Map();
        let Queue: Loc[] = [];
        let r: number;
        let c: number;
        Queue.push([startr, startc]);
        while (Queue.length > 0) {
            [r, c] = Queue.shift();
            if (r == endr && c == endc) { break; }
            Visited.set(loc2string(r, c,), true);
            for (let delta of [[0, 1], [1, 0], [0, -1], [-1, 0]]) {
                let [deltar, deltac] = delta;
                let childr = r + deltar;
                let childc = c + deltac;
                if (0 <= childr && childr < this.rows &&
                    0 <= childc && childc < this.cols &&
                    isOpen(this.board[childr][childc]) &&
                    !Visited.has(loc2string(childr, childc))) {
                    Parent.set(loc2string(childr, childc), [r, c]);
                    Queue.push([childr, childc]);
                }
            }
        }
        if (r == endr && c == endc) {
            let path: StepList = [];
            while (r != startr || c != startc) {
                let [parentr, parentc] = Parent.get(loc2string(r, c));
                let deltar = r - parentr;
                let deltac = c - parentc;
                let m: Move;
                if (deltar == 1) { m = Move.down; }
                else if (deltar == -1) { m = Move.up; }
                else if (deltac == 1) { m = Move.right; }
                else { m = Move.left; }
                path.unshift([player, m]);
                r = parentr;
                c = parentc;
            }
            return path;
        }
        else {
            myError('No path found to = ' + endr + ',' + endc);
        }
    }

    ExecuteMove(step: Step): boolean {
        // make move, return true if successful / false if illegal move
        let player = step[0];
        let move = step[1];
        let [playerStartRow, playerStartCol] = this.playerLocs[player - 1];
        if (move == Move.gotobox) {
            let box: number = step[2];
            let dir: Move = <Move>step[3]; // why did this create an error?
            // Find shortest path
            let [boxRow, boxCol] = this.boxLocs[box - 1];
            let [rdelta, cdelta] = Delta(dir);
            let playerDestRow = boxRow - rdelta;
            let playerDestCol = boxCol - cdelta;
            let plan: StepList = this.FindPath(player, playerStartRow, playerStartCol, playerDestRow, playerDestCol);
            plan.push([player, dir]);
            // Add push to plan
            // Splice the plan into the solution
            let newSol = this.solutionStepList.slice(0, this.solutionPosition);
            newSol = newSol.concat(plan);
            newSol = newSol.concat(this.solutionStepList.slice(this.solutionPosition + 1));
            this.solutionStepList = newSol;
            this.generateSolutionHTML();
            this.solutionPosition--;
            return true;
        }
        else if (move == Move.moveboxcell) {
            let cell1: number = step[2];
            let cell2: number = <number>step[3];
            let [r1, c1] = [Math.floor(cell1 / this.cols), cell1 % this.cols];
            let [r2, c2] = [Math.floor(cell2 / this.cols), cell2 % this.cols];
            let [rdelta, cdelta] = [r2 - r1, c2 - c1]
            // Find shortest path
            let playerDestRow = r1 - rdelta;
            let playerDestCol = c1 - cdelta;
            let plan: StepList = this.FindPath(player, playerStartRow, playerStartCol, playerDestRow, playerDestCol);
            // Add the push
            plan.push([player, Delta2Push(rdelta, cdelta)]);
            // Add push to plan
            // Splice the plan into the solution
            let newSol = this.solutionStepList.slice(0, this.solutionPosition);
            newSol = newSol.concat(plan);
            newSol = newSol.concat(this.solutionStepList.slice(this.solutionPosition + 1));
            this.solutionStepList = newSol;
            this.generateSolutionHTML();
            this.solutionPosition--;
            return true;
        }
        else {
            // Regular move
            let [rdelta, cdelta] = Delta(move);
            let [playerDestRow, playerDestCol] = [playerStartRow + rdelta, playerStartCol + cdelta];
            let startSquare: Square = this.board[playerStartRow][playerStartCol];
            let destSquare: Square = this.board[playerDestRow][playerDestCol];

            if (isPush(move)) {
                // confirm that there is a box to be pushed directly
                if (playerDestRow < 0 || playerDestRow >= this.rows ||
                    playerDestCol < 0 || playerDestCol >= this.cols ||
                    !isBox(this.board[playerDestRow][playerDestCol])) {
                    return false;
                }
                // find the clear space after the chain of one or more boxes
                let [boxDestRow, boxDestCol] = [playerDestRow, playerDestCol];
                let numboxes = 0;
                while (true) {
                    [boxDestRow, boxDestCol] = [boxDestRow + rdelta, boxDestCol + cdelta];
                    if (boxDestRow < 0 || boxDestRow >= this.rows ||
                        boxDestCol < 0 || boxDestCol >= this.cols) {
                        return false;
                    }
                    numboxes += 1;
                    if (!isBox(this.board[boxDestRow][boxDestCol])) {
                        break;
                    }
                }
                if (!isOpen(this.board[boxDestRow][boxDestCol])) {
                    return false;
                }
                if (this.multibox == false && numboxes > 1) {
                    return false;
                }
                // Move the boxes from last to the first while updating BoxLocs
                while (boxDestRow != playerDestRow || boxDestCol != playerDestCol) {
                    // Determine the source coordinates
                    let [boxSourceRow, boxSourceCol] = [boxDestRow - rdelta, boxDestCol - cdelta];
                    // Determine the square contents
                    let boxDestSquare = this.board[boxDestRow][boxDestCol];
                    let boxSourceSquare = this.board[boxSourceRow][boxSourceCol];
                    // Determine index of box being moved
                    let boxIndex = 0;
                    while (this.boxLocs[boxIndex][0] != boxSourceRow || this.boxLocs[boxIndex][1] != boxSourceCol) {
                        boxIndex += 1;
                    }
                    // Update the location of box in BoxLocs
                    this.boxLocs[boxIndex] = [boxDestRow, boxDestCol];
                    // Erase box from board
                    this.board[boxSourceRow][boxSourceCol] = clearSquare(boxSourceSquare);
                    // Add box to the board
                    this.board[boxDestRow][boxDestCol] = putBox(boxDestSquare);
                    // Update boxes on goal
                    if (isGoal(boxSourceSquare)) { this.numberBoxesOnGoal--; }
                    if (isGoal(boxDestSquare)) { this.numberBoxesOnGoal++; }
                    // Update the HTML for the dest box square
                    this.UpdateSquareHTML(boxDestRow, boxDestCol);
                    boxDestRow = boxSourceRow;
                    boxDestCol = boxSourceCol;
                }
                // Erase the player from board
                this.board[playerStartRow][playerStartCol] = clearSquare(startSquare);
                // Add player to board at new position
                this.board[playerDestRow][playerDestCol] = putPlayer(destSquare);
                // Update the location of the player in PlayerLocs
                this.playerLocs[player - 1] = [playerDestRow, playerDestCol];
                // Update the HTML for the prior and new player squares
                this.UpdateSquareHTML(playerDestRow, playerDestCol, move);
                this.UpdateSquareHTML(playerStartRow, playerStartCol);
                this.updateNumberBoxesOnGoal()
            }
            else { // non-push move
                if (playerDestRow < 0 || playerDestRow >= this.rows ||
                    playerDestCol < 0 || playerDestCol >= this.cols ||
                    !isOpen(this.board[playerDestRow][playerDestCol])) {
                    return false;
                }
                this.board[playerStartRow][playerStartCol] = clearSquare(startSquare);
                this.UpdateSquareHTML(playerStartRow, playerStartCol);
                this.board[playerDestRow][playerDestCol] = putPlayer(destSquare);
                this.playerLocs[player - 1] = [playerDestRow, playerDestCol];
                this.UpdateSquareHTML(playerDestRow, playerDestCol, move);
            }
            return true;
        }
    }

    updateNumberBoxesOnGoal() {
        if (usenode) return;
        document.getElementById("NumberPlayers").innerHTML = this.playerLocs.length.toString();
        document.getElementById("NumberBoxes").innerHTML = this.numberBoxes.toString();
        document.getElementById("NumberGoals").innerHTML = this.numberGoals.toString();
        document.getElementById("NumberBoxesOnGoal").innerHTML = this.numberBoxesOnGoal.toString();
    }

    initializeSolutionFromString(s: string = '', p: number = 0) {
        this.originalSolution = s;
        // Special case - if file is only numbers and whitespace
        if (/^[0-9\s]*$/.test(s)) {
            // convert to M commands
            s = s.replace(/([0-9])+\s+([0-9])+/g, "M$1,$2,");
        }
        // Filter comments
        s = s.split('\n').filter(str => str[0] != '%').join('');
        s = s.replace(/\s/g, '');
        this.solution = s;
        this.solutionStepList = this.solutionStringToStepList(s);
        this.solutionPosition = p;
        this.generateSolutionHTML();
    }

    generateSolutionHTML() {
        if (usenode) { return; }
        let multi = (this.playerLocs.length > 1);
        let h = [];
        for (let i = 0; i < this.solutionStepList.length; i++) {
            let step = this.solutionStepList[i];
            let player: number = step[0];
            let move: Move = step[1];
            let splayer = multi ? player : "";
            let tail = '';
            if (move == Move.gotobox) {
                tail = '' + step[2] + <String>step[3];
            }
            else if (move == Move.moveboxcell) {
                tail = '' + step[2] + ',' + step[3] + ',';
            }
            h.push('<span id="s-' + i + '">' +
                splayer + <String>move + tail + '</span>');
        }
        document.getElementById('solutionViewer').innerHTML = h.join('');
        (document.getElementById('stepNumberBox') as HTMLInputElement).value = (this.solutionPosition + 1).toString();
        document.getElementById('solutionHighlight').innerHTML =
            '#s-' + this.solutionPosition + ' { background-color: powderblue ; }';
    }

    playbackNextStep(): boolean {
        if (this.solutionPosition < this.solutionStepList.length) {
            if (this.ExecuteMove(this.solutionStepList[this.solutionPosition])) {
                this.solutionPosition += 1;
                // update solutionHighlight and stepNumberBox
                if (this.solutionPosition < this.solution.length) {
                    (document.getElementById('stepNumberBox') as HTMLInputElement).value = (this.solutionPosition + 1).toString();
                    document.getElementById('solutionHighlight').innerHTML =
                        '#s-' + this.solutionPosition + ' { background-color: powderblue ; }';
                }
                else {
                    (document.getElementById('stepNumberBox') as HTMLInputElement).value = '';
                    document.getElementById('solutionHighlight').innerHTML = '';
                }
            }
            else {
                myError('Illegal move at solution step ' + (this.solutionPosition + 1));
            }
        }
        else {
            myWarning('Reached end of solution');
            return false;
        }
        return true;
    }

}  // End of class Sokoban

// Global Variables

var sokoban: Sokoban;
var playbackMode: string = 'step'; // values are: step, auto, record
var delay: number = 0;

// Interface Functions

function Schedule(fn) {
    if (usenode) {
        setTimeout(fn, delay);
    }
    else {
        window.setTimeout(fn, delay);
    }
}

function NoFocus() {
    document.getElementById('FakeButton').focus({ preventScroll: true });
}

function initializeAll() {
    // Called when HTML page body loaded
    sokoban = new Sokoban(defaultPuzzle);
    sokoban.initializeSolutionFromString(defaultSolution);
    window.addEventListener("resize", updateSquareStyle);
    updateSquareStyle();
    document.addEventListener('keyup', event => {
        switch (event.code) {
            case 'Space':
                spacebarHandler();
                break;
            case 'ArrowDown':
                movePlayer(Move.down);
                break;
            case 'ArrowUp':
                movePlayer(Move.up);
                break;
            case 'ArrowLeft':
                movePlayer(Move.left);
                break;
            case 'ArrowRight':
                movePlayer(Move.right);
                break;
        }
    });
    debug && console.log('About to call createExamplesButton');
    createExamplesButton();
}

async function createExamplesButton() {
    console.log("Starting createExamplesButton")
    const puzzleFiles: string = (await (await fetch("exampleFiles.txt")).text());
    debug && console.log('contents exampleFiles.txt = ', puzzleFiles);
    const puzzleFilesList: string[] = puzzleFiles.match(/\S+/g);
    let selectInnerHTMLList: string[] = puzzleFilesList.map(s => '<option value="' + s + '">' +
        s.replace(/_/g, ' ') + '</option>\n');
    selectInnerHTMLList.unshift('<option value="">(none)</option>\n');
    document.getElementById('Examples').innerHTML = selectInnerHTMLList.join('');
    debug && console.log('Successfully set examples button to ', selectInnerHTMLList.join(''));
}


function updateSquareStyle() {
    let k: number = window.innerWidth - 275;
    let h: number = window.innerHeight; // units are px

    console.log('entering updateSquareStyle')
    console.log('h is ', h, ' k is ', k);


    let sqSize = Math.floor(Math.max(((h - 10) / (sokoban.rows + 1)) - 4, 8)); // Allow 2 px for border
    let sqSizeW = Math.floor(Math.max(((k - 10) / (sokoban.cols + 1)) - 4, 8)); // Allow 2 px for border

    if (sqSizeW < sqSize) { sqSize = sqSizeW; }

    let fontsize = Math.floor(sqSize * 0.70);
    let sqStyle = '.soko td { border: 1px solid; ' +
        'text-align: center;  ' +
        ' line-height: 0px; ' +
        'width:' + sqSize + 'px; height:' + sqSize + 'px; ' +
        'font-size:' + fontsize + 'px; }';
    document.getElementById('squareSizeStyle').innerHTML = sqStyle;
}

function movePlayer(move: Move) {
    if (playbackMode != 'record') { return; }
    let newstep: Step = [1, move];
    if (!sokoban.ExecuteMove(newstep)) {
        newstep = [1, <Move>(<string>move.toUpperCase())];
        if (!sokoban.ExecuteMove(newstep)) {
            myWarning('Illegal move');
            return;
        }
    }
    sokoban.solutionStepList.push(newstep);
    sokoban.solutionPosition = sokoban.solutionStepList.length;
    sokoban.generateSolutionHTML();
}

// Button Handlers 

function multiboxHandler(e: HTMLInputElement) {
    NoFocus();
    if (e.checked) {
        sokoban.multibox = true;
    }
    else {
        sokoban.multibox = false;
    }
}

function loadPuzzleButtonHandler(e: HTMLInputElement) {
    NoFocus();
    if (e == undefined) { return; }
    let f = e.files[0];
    if (f == undefined) { return; }
    let ls = document.getElementById('loadSolution') as HTMLInputElement;
    if (ls) {
        ls.value = '';
    }
    // Set up asynchronous load
    const reader = new FileReader();
    reader.onload = (e) => {
        const text = reader.result.toString();
        // Action to take when file is loaded
        sokoban = new Sokoban(text);
        updateSquareStyle();
        NoFocus();
    }
    reader.readAsText(f);
}

function loadSolutionButtonHandler(e: HTMLInputElement) {
    NoFocus();
    if (e == undefined) { return; }
    let f = e.files[0];
    if (f == undefined) { return; }
    // Set up asynchronous load
    const reader = new FileReader();
    reader.onload = (e) => {
        const text = reader.result.toString();
        // Action to take when file is loaded
        if (sokoban == undefined) { myError('loadSolution finds undefined sokoban!'); }
        sokoban.initializeSolutionFromString(text);
        NoFocus();
    }
    reader.readAsText(f);
}


async function loadServerPuzzleButtonHandler(puzzleName: string) {
    if (puzzleName == "") { return; }
    const puz: string = (await (await fetch("Examples/" + puzzleName + ".txt")).text());
    console.log(puz);
    sokoban = new Sokoban(puz);
    updateSquareStyle();
    const sol: string = (await (await fetch("Examples/" + puzzleName + "-sol.txt")).text());
    sokoban.initializeSolutionFromString(sol);
    NoFocus();
}




function resetPuzzlerHandler() {
    NoFocus();
    // Don't change the solution, just go to the start of solution!
    sokoban.fillPuzzle(sokoban.originalPuzzle, false);
    sokoban.solutionPosition = 0;
    sokoban.generateSolutionHTML();
}

function clearSolutionButtonHandler() {
    NoFocus();
    sokoban.fillPuzzle(sokoban.originalPuzzle, false);
    sokoban.solutionStepList = [];
    sokoban.solutionPosition = 0;
    sokoban.generateSolutionHTML();
}

function recordSolutionButtonHandler() {
    NoFocus();
    if (playbackMode == 'record') {
        playbackMode = 'step';
        document.getElementById('RecordButtonId').innerHTML = 'Record';
    }
    else {
        if (sokoban.solutionPosition < sokoban.solutionStepList.length) {
            // Clear rest of solution from this point forward
            sokoban.solutionStepList = sokoban.solutionStepList.slice(0, sokoban.solutionPosition);
            sokoban.generateSolutionHTML();
        }
        playbackMode = 'record';
        document.getElementById('RecordButtonId').innerHTML = 'Stop';
    }
}

function playbackButtonHandler(val: string) {
    NoFocus();
    // TO DO:
    // Get out of record mode if in it
    playbackMode = val;
    switch (playbackMode) {
        case 'step':
            playbackMode = 'step';
            return;
        case 'autoslow':
            playbackMode = 'auto';
            delay = 2000;
            break;
        case 'automedium':
            playbackMode = 'auto';
            delay = 1000;
            break;
        case 'autofast':
            playbackMode = 'auto';
            delay = 400;
            break;
        case 'autoturbo':
            playbackMode = 'auto';
            delay = 150;
            break;
        case 'autoludicrous':
            playbackMode = 'auto';
            delay = 0;
            break;
    }
    Schedule(autoPlayHandler);
}

function autoPlayHandler() {
    NoFocus();
    if (playbackMode != 'auto') { return; }
    if (!sokoban.playbackNextStep()) {
        playbackMode = 'step';
        let stepbutton = document.getElementById('step') as HTMLInputElement;
        stepbutton.click();
        return;
    }
    if (playbackMode != 'auto') { return; }
    Schedule(autoPlayHandler);
}

function spacebarHandler() {
    NoFocus();
    if (playbackMode == 'step') {
        sokoban.playbackNextStep();
    }
}

function backwardButtonHandler() {
    NoFocus();
    // myError('Not yet implemented');
}

function goButtonHandler() {
    NoFocus();
    // myError('Not yet implemented');
}

function solutionOnclickHandler(id: string) {
    NoFocus();
    // myError('Not yet implemented');
}

function squareHandler(id: string) {
    let sq: string, rs: string, cs: string;
    NoFocus();
    // myError('Not yet implemented');
}





