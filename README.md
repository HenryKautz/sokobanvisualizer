Sokoban Visualizer

Copyright 2020 Henry Kautz
Free for academic or research use

This program is designed to visualize machine-generated solutions to large Sokoban problems.
See help.html for details
